############################################
# Setup Server
############################################

set :stage, :staging
set :stage_url, "http://wpdeploy.nwwtest.com"
server "web596.webfaction.com", user: "dnww", roles: %w{web app db}
set :deploy_to, "/home/dnww/webapps/wpdeploy"

############################################
# Setup Git
############################################

set :branch, "develop"
